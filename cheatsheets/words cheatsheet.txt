============= EN ==============

angst
  fear, anxiety

anticlimactic (CS snad "antiklimaktický")
  disappointing after escalating foreshadowing plot (so called climax)

collateral (kolaterální)      
  related but different, a dihttps://www.slovnik-synonym.cz/web.php/slovo/ukladnyfhttps://www.slovnik-synonym.cz/web.php/slovo/ukladnyferent line, secondary

diligent
  trying to do one's duties and work well (svědomitý)

discrepancy
  rozpor

eavesdrop
  secretly listen to a conversation

hegemony (hegemonie)
  great dominance of one group over others

juvenile
  young, animal's young

pathological
  caused by physical or mental disease

secular
  not connected to religion or spirituality (světský)

============= CS ==============

aforismus
  krátké "pořekadlo", avšak s hlubším významem, chytrým humorem apod.

asanace
  ozdravování životního prostředí

avantgrada
  jeden z uměleckých směrů 20. století, "průkopníci"

hamižný
  příliš toužící po majetku a penězích

hegemonie
  politická/mocenská dominance jednoho státu (či skupiny) nad jiným bez přímého
  použití hrubé síly

ješitný
  samolibý, zakládá si na vlastních (mnohdy jen domělých) kladných vlastnostech

katarze
  povznášející pocit, očista, úleva, po zážitku např. z uměleckého díla

kibuc
  socialistická osada v Izraeli fungující na principu kolektivního vlastnictví

krédo
  vyznání víry, šířeji osobní životní zásady

letargie
  extrémní spavost, lhostejnost, netečnost

malichernost
  něco, na čem se z pohledu jednoho trvá více než je nutné, maličkost

moratorium
  odklad splátky

pleticha
  podlý kalkuk ve svůj prospěch, léčka, úskok

rustikální
  venkovský, prostý

servilní
  podlézavý, patolízalský

skoupý
  příliš šetřivý/šetrný

úkladný
  zákeřný

žoviální
  bezstarostně až povrchně veselý

 

