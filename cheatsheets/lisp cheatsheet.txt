WORK IN PROGRESS

basics:

  it's both compiled and interpreted
  use common lisp
  CCL (clojure common lisp) implementation
  source file extension: .lisp, .cl

  run: ccl -l filename

  Sketch library (https://github.com/vydd/sketch) for games - installation and usage:
    sudo apt-get install cl-quicklisp   # install package manager
    ccl                                 # start lisp
    > (load "~/quicklisp/setup.lisp")
    > (ql-dist:install-dist "http://bodge.borodust.org/dist/org.borodust.bodge.txt")
    > (ql:quickload :sketch)

  nice cheatsheet: http://jtra.cz/stuff/lisp/sclr/index.html

language:

  Expression are case-insensitive.

  s-expression (symbolic expression) - General notation for a binary tree of form
    (X Y) where X and Y are each either atom or another s-expr. E.g.

    (A ((B C) D))

       /\
      A /\
       /\ D
      B  C

    In generalized form they can also represent lists. E.g.:

    (1 2 3 4 5) = (1 (2 (3 (4 (5 NIL)))))

  LISP form - data that are also a program, e.g. (+ 1 2) is a form but
    (1 2 3) is not (can't be run as a program)

  comment: ;

  basic language elements:
    - atom:       number or symbol, e.g. 123, mysomthing, *hello*
    - list:       s-expression, e.g. (i am a list)
    - string:     text string, e.g. "I am a string." - they're actually lists too

  built-in values:
    - t:          true
    - nil:        empty list, also a false value

  data types:
    Are hierarchical, can be scalar or vector. Some of them are:
    list, atom, string, array, string, character, integer, float, rational,
    complex , null, vector, function, ...

  ' causes lists to not be avaluated as functions. E.g.:
    (write (* 2 3))       ; will write 6
    (write '(* 2 3))      ; will write: (* 2 3)

  macros vs functions:
    Macros are exapnded at compile time. Use macros for syntax, not semantics!

  Characters are denoted as #\c, e.g. 'a' = #\a. Some special characters are
    #\Tab #\Linefeed #\Return, ...

functions/commands:

  general:
    (quit)                   Quit the REPL - Write at the end of the programs.
    (listp what)             Tests if given object is a list.

  control flow:
    (if cond act1 act2)      If-then-else construct. Returns the result of the
                             evaluated branch.
    (when cond act1)         If-then construct. Returns either evaluated result
                             or NIL (if cond is not t).
    (cond                    Switch/case - tests multiple conditions like nested
      (cond1 act1)           if statements.
      (cond2 act2)
      ...)
    (case (expr)
      (val1 act1 act1b ...)  Another switch, but works a little different -
      (val2 act2 act2b ...)  evaluates an expression and depending on the value
      ...)                   performs actions.
    (loop actions)           Repeats actions until return is executed.
    (loop for var in list do For loop that iterates on a list.
      action)
    (loop for var from val1  For loop that uses a number range.
      to val2 do action)
    (dotimes (var times)     Loop (for) with fixed number of iterations.
      actions)
    (return val)             Return given value.
    (block name actions)     Defines a block of commands.
    (return-from name)       Exits from a named block (similar to exceptions).

  io:
    (write what)             Output what to console.
    (write-line what)        As write but with newline at the end.
    (print what)             Prints what with newline before and a space after.
    (pprint what)            Pretty print what.
    (write-char c)           Writes out a character c as a character.

  variables and functions:
    (defvar var val)         Defines a global variable. E.g. (defvar x 10)
    (defconstant con val)    Defines a constant with given value.
    (let var val)            Defines a local variable. E.g. (let x 10)
    (type-of what)           Returns the data type of what.
    (set var val)            Set the variable value - almost always used with
                             first argument quoted (e.g. (set 'x 10) - you can use
                             setq instead.
    (setq var val)           Equivalent to (set 'var val). E.g. (setq x 10)
    (setf var val)           Like setq but is generalized, can be used for array
                             elements etc.
    (defmacro name (params)  Define a macro.
      "documentation string"
      body)
    (defun name (params)     Defines a function.
      "doc string"
      actions)

      
  math/logical/operators:
    (= x y)                  Equality operator.
    (eq o1 o2)               Tests identity of two objects.
    (eql a b)                Tests identity or number equality.
    (/= x y)                 Inequality operator. 
    (mod x y)                Modulus operator.
    (incf x)                 Increment variable.
    (decf x)                 Decrement variable.
    (max x y)                Maximum.
    (min x y)                Minimum.
    (and a b)                And function.
    (or a b)                 Or function.
    (not a)                  Boolean negation.
    (logand a b)             Bitwise and function.
    (evenp x)                Checks if given number is even.
    (sin x)                  Sine function.
    (exp x)                  Exponential function.
    (sqrt x)                 Square root of x.
    (abs x)                  Absolute value of x.
    (round x)                Round the number x.
    (float x)                Convert nuber x to floating point.
    (realpart x)             Returns the real part of an imaginary number x.
    