This is a list of promising projects that are not in my active view scope but I want to keep an eye on.

ZeroNet              https://zeronet.io
Monnaie Libre        https://www.monnaielibreoccitanie.org
OpMon                https://github.com/OpMonTeam/OpMon
Catch Challenger     https://catchchallenger.first-world.info
TuxeMon              https://tuxemon.org
Yorg                 https://ya2.itch.io/yorg
Tanks of Freedom     https://tof.p1x.in
MAKERPhone           https://www.kickstarter.com/projects/albertgajsak/makerphone-an-educational-diy-mobile-phone
ZeroPhone            https://wiki.zerophone.org/index.php/Main_Page
UzeBox handheld      http://uzebox.org/forums/viewtopic.php?f=4&t=2198&start=90#p29816
Phil from GCHQ       https://phillfromgchq.co.uk/?phill=1
Pepper and Carrot    https://www.peppercarrot.com
Capital F            http://capitalf.org
XLEngine             https://github.com/Mindwerks/XLEngine
MNT Reform           http://mntmn.com/reform/
TinyFPGA console     https://discourse.tinyfpga.com/t/bx-portable-game-console-project-collaboration/553/135
Manyverse            https://www.manyver.se
prismo               https://gitlab.com/mbajur/prismo/
alda                 https://github.com/alda-lang/alda
Wikipedia books      https://en.wikipedia.org/wiki/Wikipedia:Books
Seed of Andromeda    https://www.seedofandromeda.com/
VOADI                https://gitlab.com/voadi/voadi
Thrive               http://revolutionarygamesstudio.com/
Automatune           https://www.automatune.com/
Librem5              https://puri.sm/products/librem-5/
Necunos NC_1 phone   https://necunos.com/shop/
32blit               https://www.kickstarter.com/projects/pimoroni/32blit-retro-inspired-handheld-with-open-source-fi
Pyra                 https://pyra-handheld.com/boards/pages/pyra/
Pine64               https://www.pine64.org
Lemmy                https://github.com/dessalines/lemmy
Collapse OS          https://collapseos.org/
Redox                https://en.wikipedia.org/wiki/Redox_(operating_system)
Pinebook Pro         https://store.pine64.org/?product=14%e2%80%b3-pinebook-pro-linux-laptop-ansi-us-keyboard-estimated-dispatch-in-december-2019
ESPBoy               https://www.espboy.com/
YaCy                 https://yacy.net/
uSVC                 https://www.crowdsupply.com/itaca-innovation/usvc
degrowth             https://degrowth.org/
Lokinet              https://lokinet.org/
LBRY                 https://lbry.com/
WAX                  https://github.com/LingDong-/wax
