This is a waiver for one's body, something aking CC0 with which you can make
your body available for touch etc. without any conditions, giving up the rights
to sue.

This is to be used as follow: you put this somewhere public with a proof it was
posted by you, i.e. guaranteeing anyone a proof you've given him the rights to
your body. Then you can for example wear a T-shirt saying something like
"You may touch me if you want <3" alongside with URL where anyone can check
(e.g. thanks to a photo) that you are serious about it. Then random people can
just go and do stuff to you.

The text of the waiver was made by me (drummyfish) and I release it under CC0
1.0, public domain.

The waiver text follows (fill in your personal details):

==================

Consent To Any Sexual Behavior

version 1.0

The intent of this text is to ensure that anyone at any time can freely interact
with my body without fear of legal action from my side, possibility of which I
hereby give up. This statement is serious and legally binding.

I, NAME, born DATE, PLACE, give an unconditional and irrevocable consent to all
affected persons to behave towards me at any time and under any circumstances in
a way that might otherwise be ruled as sexual abuse. This includes (but is not
limited to) hugging, kissing, touching any part of my body with any part of the
other person's body, verbally or otherwise communicating any messages to me,
photographing me etc.
