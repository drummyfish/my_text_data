set number
set et
set sw=2
set hlsearch
set nowrap
set colorcolumn=80
set list
set listchars=tab:>.
set backspace=indent,eol,start
syntax on
