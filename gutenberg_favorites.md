Some favorite books on project gutenberg, roughly from the most favorite to
least favorite (not strictly).

Flatland: A Romance of Many Dimensions (1884)
The Story of The Heavens (1900)
Úplná učebnice mezinárodní řeči dra. Esperanta (1890)
Bible Pictures and Stories in Large Print (1989)           <--- very nice Bible tl;dr
Great Disasters And Horror's In The World's History (1890)
Barkham Burroughs' Encyclopaedia of Astounding Facts and Useful Information (1889)
Winnetou, the Apache Knight by Karl May and Marion Ames Taggarti (1893)  <-- and other books by Karl May!
Circle of Knowledge: A Classified, Simplified, Visualized Book of Answers <--- lol, detailed "racist" table comparing races
Popular Scientific Recreations (< 1900)
The Bad Family & Other Stories (1898)
The Book of Curiosities (1854)
Alden's Handy Atlas of the World (1888)  <--- nice world maps and data
The Nuttall Encyclopaedia (1907)
Great Facts (1860)
Cities of the Dawn (1897)
The American Encyclopedia of History, Biography and Travel (1857)
A Complete Dictionary of Synonyms and Antonyms (1898)
A Woman's Journey Round the World (1850)
The Heavens Above: A Popular Handbook of Astronomy (1882)
Lives of Poor Boys Who Became Famous (1885)   <--- nice, very readable short bios of many geniuses
A Text-Book of Astronomy (1903)
The handy manual: A veritable mine of useful and interesting statistics, information, etc (1895)
The Every Day Book of History and Chronology (1858)  <--- big data set of dates with historical events
Journal of an African Cruiser (1845)  <--- nice journal of some guy
(Appletons') Popular Science Monthly (~1900) <--- pretty nice articles, also found some funny stuff about negros
Hoyle's Games Modernized (1909)
Ten Years and Ten Months in Lunatic Asylums in Different States (1874)
Thrilling Narratives of Mutiny, Murder And Piracy (probably < 1900, an ad contains 18xx dates, there is no 19xx date in the text, but many 18xx)
Useful Knowledge: Volume 3. Animals (1825)
Captain Cook's Journal During His First Voyage Round the World (1893)
The Living Animals of the World (1902)
The Life of That Wonderful and Extraordinarily Heavy Man, Daniel Lambert (1818)
Popular Technology; or, Professions and Trades (1841)
A Popular Account of the Manners and Customs of India (1847)
Extinct Monsters (1897)
A Narrative Of The Mutiny, On Board His Majesty's Ship Bounty (1790)
Knowledge For The Time (1864)
The First Six Books of the Elements of Euclid (1885)
Audubon and His Journals (1897)  <--- some bird researcher's journal, day by day
A Short History of the World (1922)
The Arabian Nights' Entertainments (1835)
Invention and Discovery: Curious Facts and Characteristic Sketches (1868)

The Square Root of 4 to a Million Places <--- lol
